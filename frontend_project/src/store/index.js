import { defineStore } from "pinia";
import Cookies from "js-cookie";

export const useStore = defineStore("app", {
	state: () => ({
		user: {},
	}),
	actions: {
		setUser(user) {
			this.user = user;
		},
		logout() {
			this.user = {};
			Cookies.remove("access_token");
		},
	},
});
