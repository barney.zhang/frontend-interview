import axios from "axios";

export const GetInoviceData = async ({ start, end }) => {
	try {
		if (start && end && typeof start === "number" && typeof end === "number") {
			return await axios.get("http://localhost:8000/api/invoice", {
				params: {
					start,
					end,
				},
			});
		} else {
			throw new Error("Params missing or format is not right.");
		}
	} catch (error) {
		console.error(error);
	}
};
