import axios from "axios";
import qs from "qs";

const CLIENT_ID = import.meta.env.VITE_LINE_CLIENT_ID;
const CLIENT_SECRET = import.meta.env.VITE_LINE_CLIENT_SECRET;
const CALLBACK_URI = import.meta.env.VITE_LINE_CALLBACK_URI;

export const GetLineAuthLink = () => {
	return `https://access.line.me/oauth2/v2.1/authorize?response_type=code&client_id=${CLIENT_ID}&redirect_uri=${encodeURI(
		CALLBACK_URI
	)}&state=1&scope=profile%20openid`;
};

export const GetLineAccessToken = async (code) => {
	const body = qs.stringify({
		grant_type: "authorization_code",
		code,
		redirect_uri: CALLBACK_URI,
		client_id: CLIENT_ID,
		client_secret: CLIENT_SECRET,
	});

	try {
		return await axios.post("https://api.line.me/oauth2/v2.1/token", body);
	} catch (error) {
		console.error(error);
	}
};

export const GetLineMeProfile = async (accessToken) => {
	try {
		return await axios.get("https://api.line.me/v2/profile", {
			headers: {
				Authorization: `Bearer ${accessToken}`,
			},
		});
	} catch (error) {
		console.error(error);
	}
};
