import { computed } from "@vue/runtime-core";
import { GetLineAccessToken, GetLineMeProfile } from "@/api/line";
import { useRoute, useRouter } from "vue-router";
import { useStore } from "@/store";
import Cookies from "js-cookie";

export default async function useLineLogin() {
	const route = useRoute();
	const router = useRouter();
	const store = useStore();

	const code = computed(() => {
		const { code } = route.query;
		return code;
	});

	async function setUserService(access_token) {
		const me = await GetLineMeProfile(access_token);
		store.setUser(me.data);
		router.replace({ name: "overview" });
	}

	if (Cookies.get("access_token") !== undefined) {
		setUserService(Cookies.get("access_token"));
	} else {
		const res = await GetLineAccessToken(code.value);
		await setUserService(res.data.access_token);

		let expires = new Date(
			new Date().getTime() + Number(res.data.expires_in * 1000)
		);
		Cookies.set("access_token", res.data.access_token, { expires });
	}

	return;
}
