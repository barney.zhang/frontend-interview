import express from "express";
import cors from "cors";
import fs from "fs";
const rawData = JSON.parse(fs.readFileSync("./rawData.json"));

const app = express();
app.use(cors());
const port = 8000;

app.get("/", (req, res) => {
	res.send("ok");
});

app.get("/api/invoice", (req, res) => {
	let { start, end } = req.query;
	start = Number(start);
	end = Number(end);
	if (!start) return res.status(400).send("start is required");
	if (!end) return res.status(400).send("end is required");
	if (start > end) return res.status(400).send("start can not larger than end");

	const data = [];
	for (let year = start; year < end + 1; year++) {
		const yearData = rawData.filter((item) =>
			item["年別"].includes(`${year}年`)
		);
		if (yearData.length) data.push(...yearData);
	}
	res.send(data);
});

app.listen(port, () => {
	console.log(`Example app listening on port http://localhost:${port}`);
});
